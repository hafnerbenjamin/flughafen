package Airport;

/**
 * Created by Benj on 03.05.2016.
 */
public class DHL {

    public int getAnzahlPakete() {
        return anzahlPakete;
    }

    public void setAnzahlPakete(int anzahlPakete) {
        this.anzahlPakete = anzahlPakete;
    }

    private int anzahlPakete;


    public void paket(int anzahl){
        setAnzahlPakete(anzahl);
        System.out.println("Es wurden " + getAnzahlPakete() + " Packete abgeholt");
    }
}
