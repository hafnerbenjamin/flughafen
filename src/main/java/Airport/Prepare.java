package Airport;

import client.UI;
import flight.CargoFlight;
import flight.Flight;
import passenger.Passenger;

/**
 * Created by Benj on 03.05.2016.
 */
public class Prepare {

    public void newFlight(){
        UI ui = new UI();
        Flight f = ui.mainMenue();

        f.setPilot(ui.pilotMenue());

        Reful reful = new Reful();
        reful.aircraft();

        if(f instanceof CargoFlight){
            DHL dhl = new DHL();
            dhl.paket(ui.dhlMenue());
            f.setAnzahlPakete(dhl.getAnzahlPakete());
        }

        Passenger passenger = new Passenger();
        passenger.setAnzahl(ui.passengerMenu());
        f.setPassenger(passenger);
        passenger.einsteigen();

        f.takeOff();

        newFlight();
    }
}
