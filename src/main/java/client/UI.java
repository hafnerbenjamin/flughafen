package client;

import flight.CargoFlight;
import flight.Flight;
import flight.PassangerFlight;
import pilot.Pilot;

import java.util.Scanner;

/**
 * Created by Benj on 06.05.2016.
 */
public class UI {

    private Scanner scan = new Scanner(System.in);

    public int passengerMenu(){
        System.out.println("Wie viele Passagiere möchten sie mitnehmen?");
        return scan.nextInt();
    }

    public Flight mainMenue() {
        System.out.println("" +
                "0 Exit" + "\n" +
                "1 für neues Passagierflugzeug" + "\n" +
                "2 für neues Cargoflugzeug");

        int in = scan.nextInt();
        if (in == 1) {
            System.out.println("Flugzeugnr?");
            Flight f = new PassangerFlight(scan.next());
            return f;

        } else if (in == 2) {
            System.out.println("Flugzeugnr?");
            Flight f = new CargoFlight(scan.next());
            return f;
        } else if (in == 0){
            System.exit(0);
        }

        Flight f = new CargoFlight("");
        return f;

    }

    public Pilot pilotMenue(){
        System.out.println("PilotenName?");
        Pilot pilot = new Pilot(scan.next());
        return pilot;
    }

    public int dhlMenue(){
        System.out.println("Wie viele Pakete möchten Sie mit nehmen?");
        return scan.nextInt();
    }


}
