package flight;

/**
 * Created by grazi on 26.04.16.
 */
public class CargoFlight extends Flight {
    public CargoFlight(String FlightNr) {
        super(FlightNr);
    }

    @Override
    public void takeOff(){
        System.out.println("Das Flugzeug " + super.getFlightNr() +
                " mit dem Pilot " + super.getPilot().getName() +
                " hat " + super.getAnzahlPakete() + " Packete aufgeladen und " +
                " ist mit " + super.getPassenger().getAnzahl() + " Passagieren abgehoben");

    }

}
