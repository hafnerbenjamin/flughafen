package flight;

import passenger.Passenger;
import pilot.Pilot;

/**
 * Created by grazi on 26.04.16.
 */
public abstract class Flight {



    private String flightNr;
    private Pilot pilot;
    private Passenger passenger;
    private int anzahlPakete;

    public int getAnzahlPakete() {
        return anzahlPakete;
    }

    public void setAnzahlPakete(int anzahlPakete) {
        this.anzahlPakete = anzahlPakete;
    }


    public String getFlightNr() {
        return flightNr;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Pilot getPilot() {
        return pilot;
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public Flight(String flightNr){
        this.flightNr = flightNr;
    }

    abstract public void takeOff();
}
