package flight;

/**
 * Created by Benj on 03.05.2016.
 */
public class PassangerFlight extends Flight {

    public PassangerFlight(String FlightNr) {
        super(FlightNr);
    }

    @Override
    public void takeOff(){
        System.out.println("Das Flugzeug " + super.getFlightNr() +
                " mit dem Pilot " + super.getPilot().getName() +
                " ist mit " + super.getPassenger().getAnzahl() + " Passagieren abgehoben");

    }

}
