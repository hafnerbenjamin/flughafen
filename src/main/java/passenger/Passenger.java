package passenger;

/**
 * Created by grazi on 26.04.16.
 */
public class Passenger {
    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    private int anzahl;

    public void einsteigen() {
        System.out.println("Es sind " + getAnzahl() + " eingeistiegen");
    }
}
