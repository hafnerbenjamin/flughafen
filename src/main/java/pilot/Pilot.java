package pilot;

/**
 * Created by grazi on 26.04.16.
 */
public class Pilot {

    private String name;

    public Pilot(String name) {
        setName(name);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
